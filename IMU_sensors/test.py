import time
import adxl345
import itg3200


accel = adxl345.SensorADXL345(1, 0x53)
gyro = itg3200.SensorITG3200(1, 0x68)

accel.default_init()
gyro.default_init()

while 1: 
     ax, ay, az = accel.read_data()
     accel.standby()
     time.sleep(0.25)

     gx, gy, gz = gyro.read_data()
     time.sleep(0.25)
    
     print(ax, ay, az, gx, gy, gz)
