from PyMata.pymata import PyMata
import time





def voltData(read):
    #initial values
    vPow = 5.0 #arduino power
    r1 = 2160.0 #resistor 1 value
    r2 = 1216.0 #resistor 2 value
    vDiv = 0.360
    vHigh = 0.0
    vLow = 0.0
    socHigh = 0.0
    socLow = 0.0
    readingValues = [(14.4, 1.0),
                    (14.0, .99),
                    (13.6, .99),
                    (13.2, .31),
                    (12.8, .08),
                    (12.4, .04),
                    (12.0, .02)
                    ]
    v = (read * vPow) / 1024.0 / vDiv + 0.23
    print(v)
    for i in readingValues:
        if v < i[0]:
            vHigh = i[0]
            socHigh = i[1]
        else:
            vLow = i[0]
            socLow = i[1]
        if vLow > 0:
            break
    print(vHigh)
    print(vLow)
    chargeState = socLow + (v - vLow)*((socHigh - socLow)/(vHigh - vLow))
    return chargeState


if __name__ == "__main__":
    vMtr = 0
    board = PyMata("dev/ttyACM0",verbose=True)
    board.set_pin_mode(vMtr,board.INPUT,board.ANALOG)
    


    while 1:
        read = board.analog_read(vMtr)    
        print(voltData(read))
        time.sleep(0.1)
