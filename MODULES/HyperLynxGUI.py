'''
Filename:   HyperLynxGUI.py
Author:     Nathan James, Ben Pepper, Zack Forbing, Caleb Cowen
Date:       10/18/16

Description:
    Class setting up the GUI for the Hyperlynx
    pod controls and instrumentation

'''


from Tkinter import *
import ttk
import time
import random
# from pod import Pod
# from podSensors import PodSensors
# from LinearActuator import LinearActuator

class HyperLynxGUI():

    def __init__(self, root, *args, **kwargs):

        # self.pod = Pod()
        self.trackDefault = StringVar()
        self.endpointDefault = StringVar()
        self.brakeDefault = StringVar()

        self.current_tab = None
        self.tabs = {}
        # self.sensors = PodSensors()

        notebook = ttk.Notebook(root, width=1500, height=900)
        notebook.pack()

        # Calibration State tab
        initialization = ttk.Frame(notebook)
        notebook.add(initialization, text='Initialization')

        self.start_button = ttk.Button(initialization, command= lambda: self.start_initialization(initialization, pre_flight, flight, notebook), text="START")
        self.start_button.pack()

        #pre flight tab
        pre_flight = ttk.Frame(notebook)
        notebook.add(pre_flight, text = 'Pre Flight')

        # Flight State tab
        flight = ttk.Frame(notebook)
        notebook.add(flight, text = 'Flight')

        #show flight, speed and distance

        # Braking State tab
        # braking = ttk.Panedwindow(notebook, orient = HORIZONTAL)
        # notebook.add(braking, text = 'Braking')

        # controls = ttk.Frame(braking, width=100, height=1000)
        # instruments = ttk.Frame(braking, width=500)

        # braking.add(controls, weight=1)
        # controls.config(relief=RAISED)
        # braking.add(instruments, weight=5)
        # instruments.config(relief=RAISED)


        ## Control Variables
        brake = IntVar()
        fDF = IntVar()
        rDF = IntVar()
        man = BooleanVar()

        # brakeForce = Spinbox(controls,from_ = 0, to_ = 200, textvariable = brake).pack()
        # fDwnFrc = ttk.Scale(controls, orient = VERTICAL, length = 100, variable = fDF, from_ = 0.0, to_ = 200.0).pack()
        # rDwnFrc = ttk.Scale(controls, orient = VERTICAL, length = 100, variable = rDF, from_ = 0.0, to_ = 200.0).pack()
        #
        # manCtrl = ttk.Checkbutton(controls, text="Manual", variable= man)
        # manCtrl.pack()


        ## Instrument Variables
        Acceleration = DoubleVar()
        Speed = DoubleVar()
        Temperature = DoubleVar()

        # Label for flight
        # Label for speed
        # Label for temp (maybe in right column)
        # graph using matplotlib

        self.continue_button = ttk.Button(initialization, command= lambda: self.start_pre_flight(pre_flight, flight, notebook), text="CONTINUE TO PRE-FLIGHT")

        self.everythingElseFrame = Frame(flight)

        self.trackDistance = ttk.Progressbar(self.everythingElseFrame, mode='determinate')

        self.brakeActuator = Canvas(self.everythingElseFrame,
                                    width=60, height=100,
                                    bg='lightblue')

        self.brakeRectangle = self.brakeActuator.create_rectangle(0, 100, 60, 200, fill='blue', outline='blue')

        self.brakeActuator.create_rectangle(15, 44, 44, 56, fill='white', outline='white')

        self.brakeText = self.brakeActuator.create_text(30, 50, text="0")

        self.FrontDFA = Canvas(self.everythingElseFrame,
                               width=60, height=100,
                               bg='lightblue')

        self.FrontDFARectangle = self.FrontDFA.create_rectangle(0, 100, 60, 200, fill='blue', outline='blue')


        self.FrontDFA.create_rectangle(15, 44, 44, 56, fill='white', outline='white')
        self.FrontDFAText = self.FrontDFA.create_text(30, 50, text="0")

        self.RearDFA = Canvas(self.everythingElseFrame,
                              width=60, height=100,
                              bg='lightblue')

        self.RearDFARectangle = self.RearDFA.create_rectangle(0, 100, 60, 200, fill='blue', outline='blue')

        self.RearDFA.create_rectangle(15, 44, 44, 56, fill='white', outline='white')
        self.RearDFAText = self.RearDFA.create_text(30, 50, text="0")



    def checkMessages(self, messages):
        results = []
        for message in messages:
            if 'Failure' in message['text']:
                results.append(False)
            else:
                results.append(True)
        if results.__contains__(False):
            return False
        else:
            return True

    def getData(self):
        accl = [(0.02341267, 0.9999999, 0.82852835682358)]
        gyro = [(501, -650.0, 420.69)]
        temp = 50
        pressure = 20
        sensor_data = [accl, gyro, temp, pressure]
        return sensor_data

    def check_accl(self, accl, initialization):
        message_success = Message(initialization, text="Acceleration: Success - %s m/s" % str(accl), width=500, bg='green')
        message_failure = Message(initialization, text="Acceleration: Failure - %s m/s" % str(accl), width=500, bg='red')
        message_no_input = Message(initialization, text="Acceleration: Failure - No Input", width=500, bg='red')

        if 0 < accl < 1:
            return message_success
        elif accl:
            return message_failure
        else:
            return message_no_input

    def check_gyro(self, gyro, initialization):
        message_success = Message(initialization, text="Gyroscope: Success - %s deg/s" % str(gyro), width=500, bg='green')
        message_failure = Message(initialization, text="Gyroscope: Failure - %s deg/s" % str(gyro), width=500, bg='red')
        message_no_input = Message(initialization, text="Gyroscope: Failure - No Input", width=500, bg='red')
        gyro_check = []
        for gyroscope in gyro:
            if -1000.0 <= gyroscope <= 1000.0:
                gyro_check.append(1)
            else:
                gyro_check.append(0)
        if sum(gyro_check) == 3:

            return message_success
        elif gyro:
            return message_failure
        else:
            return message_no_input

    def check_temp(self, temp, initialization):
        message_success = Message(initialization, text="Temperature: Success - %s C" % str(temp), width=500, bg='green')
        message_failure = Message(initialization, text="Temperature: Failure - %s C" % str(temp), width=500, bg='red')
        message_no_input = Message(initialization, text="Temperature: Failure - No Input", width=500, bg='red')
        if 0 <= temp <= 90:
            return message_success
        elif temp:
            return message_failure
        else:
            return message_no_input

    def check_pressure(self, pressure, initialization):
        message_success = Message(initialization, text="Pressure: Success - %s Pa" % str(pressure), width=500, bg='green')
        message_failure = Message(initialization, text="Pressure: Failure - %s Pa" % str(pressure), width=500, bg='red')
        message_no_input = Message(initialization, text="Pressure: Failure - No Input", width=500, bg='red')
        if 0 <= pressure <= 120000:
            return message_success
        elif pressure:
            return message_failure
        else:
            return message_no_input

    def start_initialization(self, initialization, pre_flight, flight, notebook):
        self.start_button.config(state=DISABLED)
        messages         = []
        # sensor_data      = self.sensors.getData()
        sensor_data      = self.getData()
        accl_message     = self.check_accl(sensor_data[0][0][0], initialization)
        gyro_message     = self.check_gyro(sensor_data[1][0], initialization)
        temp_message     = self.check_temp(sensor_data[2], initialization)
        pressure_message = self.check_pressure(sensor_data[3], initialization)
        # dfa_message      = LinearActuator.checkPos()
        accl_message.pack()
        gyro_message.pack()
        temp_message.pack()
        pressure_message.pack()
        messages.extend((accl_message, temp_message, pressure_message))
        if self.checkMessages(messages):
            self.continue_button.pack()

    def start_pre_flight(self, pre_flight, flight, notebook):
        self.continue_button.config(state=DISABLED)
        self.switchTab(pre_flight, notebook)
        length_label = Label(pre_flight, text="Track Length (in feet)")
        length_label.pack()
        length_input = Entry(pre_flight, textvariable=self.trackDefault)
        self.trackDefault.set("5280")
        length_input.pack()
        endpoint_label = Label(pre_flight, text="Target Endpoint (in feet)")
        endpoint_label.pack()
        endpoint_input = Entry(pre_flight, textvariable=self.endpointDefault)
        self.endpointDefault.set(str(5280 * .9))
        endpoint_input.pack()
        brake_g_label = Label(pre_flight, text="Max Brake Gs")
        brake_g_label.pack()
        brake_g_input = Entry(pre_flight, textvariable=self.brakeDefault)
        self.brakeDefault.set("5")
        brake_g_input.pack()
        dfa_activate_button = Checkbutton(pre_flight, text="DFA Activation")
        dfa_activate_button.pack()
        dfa_brake_activate_button = Checkbutton(pre_flight, text="DFA Braking")
        dfa_brake_activate_button.pack()

        next_button = ttk.Button(pre_flight, command= lambda: self.start_flight(flight, notebook, length_input, endpoint_input, brake_g_input, next_button), text="Start Flight")
        next_button.pack()

    def start_flight(self, flight, notebook, length_input, endpoint_input, brake_g_input, next_button):
        next_button.config(state=DISABLED)

        self.trackDefault.set(str(length_input.get()))
        self.endpointDefault.set(str(endpoint_input.get()))
        self.brakeDefault.set(str(brake_g_input.get()))
        self.switchTab(flight, notebook)
        #tell pi to go here with endpoint_input, length_input, and brake_g

        graphFrame = Frame(flight, height=400)
        graphFrame.pack(pady=5, anchor=N, fill=X)
        self.everythingElseFrame.pack(fill=X)

        velocityTimeDistanceGraph = Canvas(graphFrame, height=400)
        velocityTimeDistanceGraph.pack(pady=5, anchor=N, fill=X)

        self.trackDistance.config(maximum=int(self.trackDefault.get()))
        self.trackDistance.pack(fill=X)
        trackDistanceLabel = Label(self.everythingElseFrame, text="Track Length: %s" % str(length_input.get()))
        trackDistanceLabel.pack()

        brake_g_label = Label(self.everythingElseFrame, text="Brake Gs:")
        brake_g_label.pack()

        dfa_g_label = Label(self.everythingElseFrame, text="DFA Gs:")
        dfa_g_label.pack()

        brakeActuatorLabel = Label(self.everythingElseFrame, text='Brake Actuator')
        brakeActuatorLabel.pack(side=LEFT)
        self.brakeActuator.pack(side=LEFT)

        FrontDFALabel = Label(self.everythingElseFrame, text='Front DFA')
        FrontDFALabel.pack(side=LEFT)
        self.FrontDFA.pack(side=LEFT)

        RearDFALabel = Label(self.everythingElseFrame, text='Rear DFA')

        RearDFALabel.pack(side=LEFT)
        self.RearDFA.pack(side=LEFT)

        abort_button = Button(self.everythingElseFrame, command= lambda: self.fullstop(abort_button), text="ABORT!!!!!")
        abort_button.pack()

        root.after(50, self.update_flight, flight, notebook)

    def fullstop(self, abort_button):
        abort_button.config(state=DISABLED)
        # self.pod.fullstop() #real call
        print('stopped') #dummy method

    def update_flight(self, flight, notebook):
        self.trackDistance.step(100)

        randomNum = round(random.uniform(0, 2), 2)#needs to be getPos. Call self.pod.masterBrake.getPos
        yStartB = 100 - (randomNum / 2 * 100)

        self.brakeActuator.itemconfig(self.brakeText, text=randomNum)
        y = yStartB - self.brakeActuator.coords(self.brakeRectangle)[1]
        self.brakeActuator.move(self.brakeRectangle, 0, y)

        randomNumF = round(random.uniform(0, 4), 2) #needs to be getData
        yStartDFA = 100 - (randomNumF/4 * 100)


        self.FrontDFA.itemconfig(self.FrontDFAText, text=randomNumF)
        y = yStartDFA - self.FrontDFA.coords(self.brakeRectangle)[1]
        self.FrontDFA.move(self.FrontDFARectangle, 0, y)

        randomNumR = round(random.uniform(0, 4), 2)
        yStartDFA = 100 - (randomNumR/4 * 100)

        self.RearDFA.itemconfig(self.RearDFAText, text=randomNumR)
        y = yStartDFA - self.RearDFA.coords(self.RearDFARectangle)[1]
        self.RearDFA.move(self.RearDFARectangle, 0, y)

        root.after(200, self.update_flight, flight, notebook)

    def switchTab(self, tab, notebook):
        notebook.select(tab)

if __name__ == "__main__":
    root = Tk()
    root.title('Hyperlynx Controls')
    HyperLynxGUI(root)#.pack(side="top", fill="both", expand=True)
    root.mainloop()
