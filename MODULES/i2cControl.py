#
# I2C controller
# Written by John Gorzynski and Nathan James
# January 27th, 2017

import time
from smbus import SMBus

class I2CControl():

    def __init__(self, address, bus=None, ):
        if bus != None:
            self.bus = bus
        else:
            self.bus = SMBus(1)
        self.address = address
        
        
        # self.pinType = pinType
        # self.pwm = pwm
        
     
    
    def write(self, cmd):
        self.bus.write_byte(self.address, cmd)
    
    def read(self):
        self.bus.read_byte(self.address)
    
    # def analogWrite(pin, power):
    #       bus.write_byte(self.address, pin, power)

    # def digitalWrite(pin, power):
    #       bus.write_byte(self.address, pin, power)

    # def analogRead(pin):
    #       val = bus.read_byte(self.address, pin, power)
    #       return val;

    # def digitalRead(pin):
    #       val = bus.read_byte(self.address, pin, power)
    #       return val;

    # def setPinMode(self.address, pin, pwm, digital_or_analog)
    #       bus.write_byte(self.address, pin, value)
                 
    # def returnString(self.address, value, length)
    #       bus.read_i2c_block_data(self.address, 0x00, length)

    # def returnInt(self.address, value, length)
    #       bus.read_i2c_block_data(self.address, 0x00, length)

    # def returnfloat(self.address, value, length)
    #       bus.read_i2c_block_data(self.address, 0x00, length)
