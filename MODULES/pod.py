'''
Filename:   pod.py
Author:     Nathan James
Date:       10/27/16

Description:
    Creates the HyperLynxPod class that assembles all the sensors and controllers
    together for easy and concise manipulation.

default settings = {'Arduino':"/dev/ttyACM0",
                'fDFA':(5,6,3),
                'rDFA':(9,10,4),
                'brake':(3,11,5),
                'power':255,
                'spos':0,
                'fpos':300
                }

'''

import time
from datetime import datetime
import signal
import sys
from linearActuator import LinearActuator
from PyMata.pymata import PyMata
from podSensors import PodSensors
#from HyperLynxGUI import HyperLynxGUI




class Pod():
    def __init__(self,settings=None):
        # default settings
        if settings == None:
            self.settings = {'Arduino':"/dev/ttyACM0",
                             'fDFA':(5,6,3),
                             'rDFA':(9,10,4),
                             'brake':(3,11,5),
                             'power':255,
                             'spos':0.0,
                             'fpos':4.0,
                             'maxB': 1.5,
                             'vMtr':0
                            }
        else:
            self.settings = settings

        # Arduino board setup
        self.board = PyMata(self.settings['Arduino'], verbose=True)

        # Front DFA motor setup
        self.board.set_pin_mode(self.settings['fDFA'][0], self.board.PWM, self.board.DIGITAL)
        time.sleep(1)
        self.board.set_pin_mode(self.settings['fDFA'][1], self.board.PWM, self.board.DIGITAL)
        time.sleep(1)
        self.board.set_pin_mode(self.settings['fDFA'][2], self.board.INPUT, self.board.ANALOG)
        time.sleep(1)
        self.frontDFA = LinearActuator(self.board, self.settings['fDFA'][0],
                        self.settings['fDFA'][1], self.settings['fDFA'][2])

        # Rear DFA motor setup
        self.board.set_pin_mode(self.settings['rDFA'][0], self.board.PWM, self.board.DIGITAL)
        time.sleep(1)
        self.board.set_pin_mode(self.settings['rDFA'][1], self.board.PWM, self.board.DIGITAL)
        time.sleep(1)
        self.board.set_pin_mode(self.settings['rDFA'][2], self.board.INPUT, self.board.ANALOG)
        time.sleep(1)
        self.rearDFA = LinearActuator(self.board, self.settings['rDFA'][0],
                        self.settings['rDFA'][1], self.settings['rDFA'][2])

        # Brake motor setup
        self.board.set_pin_mode(self.settings['brake'][0], self.board.PWM, self.board.DIGITAL)
        time.sleep(1)
        self.board.set_pin_mode(self.settings['brake'][1], self.board.PWM, self.board.DIGITAL)
        time.sleep(1)
        self.board.set_pin_mode(self.settings['brake'][2], self.board.INPUT, self.board.ANALOG)
        time.sleep(1)
        self.masterBrake = LinearActuator(self.board, self.settings['brake'][0],
                            self.settings['brake'][1], self.settings['brake'][2])

        # Sensor setup
        self.sensors = PodSensors()
        self.board.set_pin_mode(self.settings['vMtr'],self.board.INPUT,self.board.ANALOG)
        
        # Class variables
        self.acceleration = [0.0, 0.0, 0.0]
        self.rotation = [0.0, 0.0, 0.0]
        self.velocity = 0.0
        self.position = 0.0
        self.temperature = 0.0
        self.pressure = 0.0
        self.soc = 0.0
        
    def signal_handler(self, sig, frame):
        print('You pressed Ctrl+C')
    
        if self.board is not None:
            self.board.reset()
        sys.exit(0)
        
    def getSOC(self):
        self.soc = self.sensors.voltData(self.board.analog_read(
                                            self.settings['vMtr']))
        return self.soc
        
    def downForce(self, mode, val=None, motors=("front","rear")):
        motor = []                              # container for motors to use
        timeout = 5.0
        power = self.settings['power']

        if "front" in motors:
            motor.append(self.frontDFA)         # add front motor
        if "rear" in motors:
            motor.append(self.rearDFA)          # add rear motor

        if mode.upper() == 'POWER':
            if val != None:                     # if no value passed
                power = val                     # use default setting
            for i in motor:                     # loop through motor list
                if val > 0:
                    i.motorF(0)
                    i.motorR(val)
                elif val < 0:
                    i.motorF(abs(val))
                    i.motorR(0)
                else:
                    i.stop()

            time.sleep(timeout)
            for i in motor:
                i.stop()

        elif mode.upper() == 'POSITION':        # need to add error for empty val
            startime = time.time()
            offset = self.frontDFA.settings['plmn']
            for i in motor:                     # loop through motor list
                if val < i.getPos():            ## start motors moving in
                    i.motorF(0)                 ## desired direction
                    i.motorR(power)             
                elif val > i.getPos():
                    i.motorF(power)
                    i.motorR(0)

            while motor and (time.time() - startime) < timeout:
               for i in xrange(len(motor)):
                   if val-offset <= motor[i].getPos() <= val+offset:
                       motor[i].stop()
                       motor.pop(i)



    def braking(self, pos=None, power=None):
        if power == None:
            power = self.settings['power']
        if pos != None:
            self.masterBrake.movePos(pos,power)
        else:                                   # no motor stop case here :(
            if power > 0:
                self.masterBrake.motorF(power)
                self.masterBrake.motorR(0)
            elif power < 0:
                self.masterBrake.motorF(0)
                self.masterBrake.motorR(abs(power))
            else:
                self.masterBrake.motorF(0)
                self.masterBrake.motorR(0)



    def readSensors(self,sense=None):
        data = []
        if sense != None:
            if "A" in sense:
                for i in self.sensors.getAccl(): 
                    data.append(i)
                    # self.acceleration.append(i)
                    # self.acceleration.pop(0)
            if "G" in sense:
                for i in self.sensors.getGyro(): 
                    data.append(i)
                    # self.rotation.append(i)
                    # self.rotation.pop(0)
            if "T" in sense:
                t = self.sensors.getTemp()
                data.append(t)
                # self.temperature = t
            if "P" in sense:
                p = self.sensors.getPres()
                data.append(p)
                # self.pressure = p
        else:
            data = self.sensors.getData()
        
            # for i in xrange(3):
            #     self.acceleration.append(data[0][i])
            #     self.acceleration.pop(0)
            #     self.rotation.append(data[1][i])
            #     self.rotation.pop(0)
            # # self.velocity =
            # # self.position =
            # self.temperature = data[2]
            # self.pressure = data[3]
        
        
        return data



    def setZero(self, offset, sense="A"):
        index = []
        if "A" in sense:
            index.append(0)
            index.append(1)
            index.append(2)
        if "G" in sense:
            index.append(3)
            index.append(4)
            index.append(5)
        if "T" in sense: index.append(6)
        if "P" in sense: index.append(7)

        self.sensors.tare(offset, index) if index else self.sensors.tare(offset)


    def destroy(self):
        self.sensors.destroy()
        self.board.reset()
        sys.exit(0)

    # def fullstop(self):
        # self.frontDFA.movePos(0) #numbers need to be validated
        # self.rearDFA.movePos(0)
        # self.brakeMaster.movePos(2)






if __name__ == "__main__":
    #signal.signal(signal.SIGINT, signal_handler)

    settings = {'Arduino':"/dev/ttyACM0",
                'fDFA':(5,6,3),
                'rDFA':(9,10,4),
                'brake':(3,11,5),
                'power':255,
                'spos':0,
                'fpos':300
                }

    data = []
    count = 0
    term = 30
    brk = settings['power']
    pwr = settings['power']
    TestLynx = Pod(settings)

    while count < term*100:
        print(TestLynx.readSensors())
        if count % 300 == 0:
            TestLynx.braking(brk)
            brk = -brk
        if count % 500 == 0:
            TestLynx.downForce('POWER', pwr)
            pwr = -pwr
        
        count += 1
        time.sleep(0.01)

    TestLynx.destroy()
    print("-= END =-")
