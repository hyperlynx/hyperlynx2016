# Class for Controlling Linear Actauators through PyMata
# to Arduino for HyperLynx. A PyMata object is needed to
# be passed into the class to utilize this class
# Written by Nathan James and Bejan Akhavan
# 9/20/16 - modfied 10/8/16

from i2cControl import I2CControl
import time

class LinearActuator(I2CControl):

#########################################################
    def __init__(self, forward, reverse, halt, position, address, bus=None, settings=None):
        I2CControl.__init__(self, address, bus)
        self.forward = forward          # command for forward
        self.reverse = reverse          # command for reverse
        self.halt = halt                # command for stop
        self.position = position        # command for position
        
        if settings == None:
            self.settings = {'spos':0.28,
                             'sanalog':2,
                             'fpos':1.73,
                             'fanalog':863,
                             'plmn':0.125
                            }
        
        
#########################################################
#########################################################


#########################################################
    def motorF(self): #default speed is max
        self.write(self.forward)

#########################################################
# motorF is Motor Forward method it takes input int
# from 0-255 and writes the PWM signal to the Arduino
# pin for forward
#########################################################


#########################################################
    def motorR(self): #default speed is max
        self.write(self.reverse)
#########################################################
# motorR is Motor Reverse method: it takes input int
# from 0-255 and writes the PWN signal to the Arduino
# pin for reverse
#########################################################


#########################################################
    def getPos(self):
        self.write(self.position)
        time.sleep(0.01)
        return self.read()
        # length = (self.settings['fpos'] - (self.settings['fanalog'] -
        #         #self.board.analog_read(self.position))
        #         self.setPinMode(self.address, self.pin, self.pwm, self.pinType) *
        #         (self.settings['fpos'] - self.settings['spos'])/
        #         (self.settings['fanalog'] - self.settings['sanalog']))
        # return length ##self.board.analog_read(self.position)
#########################################################
# getPos returns the position of the motor using the
# motor's potentiometer
#########################################################


#########################################################
    # def movePos(self, pos, spd=255): # 255 is hex for 100% open
    #     current_position = self.getPos()
    #     old_position = None

    #     while (not(pos-self.settings['plmn'] <= current_position <= 
    #             pos+self.settings['plmn']) and current_position != old_position):
    #         if current_position < pos:
    #             self.motorF(spd)
    #             time.sleep(.2)
    #         elif current_position > pos:
    #             self.motorR(spd)
    #             time.sleep(.2)
    #         old_position = current_position
    #         current_position = self.getPos()
    #     self.stop()
#########################################################
# movePos moves the motor to the position pos passed into
# the function
#########################################################



#########################################################
    def stop(self):
        self.write(self.stop)

#########################################################
# stop method stops the motor in either direction
#########################################################




if __name__ == "__main__":
    from smbus import SMBus
    hyperbus = SMBus(1)
    arduino = 0x09
    
    brake = LinearActuator(0x01,0x02,0x03, 0x04, arduino, hyperbus)
    front = LinearActuator(0x11,0x12,0x13, 0x14, arduino, hyperbus)
    rear  = LinearActuator(0x21,0x22,0x23, 0x24, arduino, hyperbus)
    
    brake.motorF()
    time.sleep(3)
    brake.motorR()
    time.sleep(3)
    
    a = input("press any key to continue ")
    
    front.motorF()
    time.sleep(3)
    front.motorR()
    time.sleep(3)
    
    a = input("press any key to continue ")
    
    rear.motorF()
    time.sleep(3)
    rear.motorR()
    time.sleep(3)
    
    #board = PyMata("/dev/ttyACM0", verbose=True)
    # self.setPinMode(self.address, 9, PWM, DIGITAL)
    # self.setPinMode(self.address, 10, PWM, DIGITAL)
    # self.setPinMode(self.address, 0, INPUT, ANALOG)
    #board.set_pin_mode(9,board.PWM,board.DIGITAL)
    #board.set_pin_mode(10,board.PWM,board.DIGITAL)
    #board.set_pin_mode(0,board.INPUT,board.ANALOG)
    
    # test = LinearActuator(board,9,10,0)
    
    # print("Current Position:")
    # print(test.getPos())
    
    # position = input("Enter position to move to (inches):")
    # test.movePos(position)
    
    # while position >= 0:
    #     print("Current Position:")
    #     print(test.getPos())
        
    #     position = input("Enter position to move to (inches):")
    #     test.movePos(position)
    
    
    print("Exiting Now")
    #test.board.close()
