'''
Filename:	podSensors.py
Author:		Nathan James w/ John Gorzynski's help :)
Date:		10/24/16

Description:
	Creates PodSensors class which handles reading data from
	Hyperlynx pod sensors.

'''

from __future__ import print_function
import time
from datetime import datetime
import smbus
import re
#import os
import adxl345
import itg3200
import BMP085
hyperbus = SMBus(1)
arduino = 0x42
value = 0;
stuff = 0;
tachoTime = []
tachoVelo = []
tachoPosition = []

class PodSensors():

	DOF = 8

	def __init__(self):
		self.accel = adxl345.SensorADXL345(1, 0x53)
		self.gyro = itg3200.SensorITG3200(1, 0x68)
		self.bmp = BMP085.BMP085()
		self.tachoBus = smbus.SMBus(1)

		self.accel.default_init()
		self.gyro.default_init()

		self.DOF = 8                 # degrees of freedom
		self.ADXLOFFSET = 256.0      # LSB offset accelerometer
		self.ITGOFFSET  = 14.375     # LSB offset gyro

		self.zero = []

		timestamp = time.ctime()
		timestamp = re.sub(r":+", '-', timestamp)
		self.data_log = open("data_log/%s pod run.dat" % timestamp, 'w+')

		for i in xrange(self.DOF): self.zero.append(0.0)

	def getAccl(self):
		ax, ay, az = self.accel.read_data()
		self.accel.standby()
		ax = ax/self.ADXLOFFSET - self.zero[0]
		ay = ay/self.ADXLOFFSET - self.zero[1]
		az = az/self.ADXLOFFSET - self.zero[2]
		# self.writeData("accl_data.csv", ax, ay, az)
		return ax, ay, az

	def getGyro(self):
		gx, gy, gz = self.gyro.read_data()
		gx = gx/self.ITGOFFSET - self.zero[3]
		gy = gy/self.ITGOFFSET - self.zero[4]
		gz = gz/self.ITGOFFSET - self.zero[5]
		# self.writeData("gyro_data.csv", gx, gy, gz)
		return gx, gy, gz

	def getTemp(self):
		temp = self.bmp.read_temperature() - self.zero[self.DOF-2]
		# self.writeData("temp_data.csv", temp)
		return temp

	def getPres(self):
		pressure = float(self.bmp.read_pressure()) - self.zero[self.DOF-1]
		# self.writeData("pressure_data.csv", pressure)
		return pressure
	
	# def getTachos(self):
			
	def voltData(self, read):
		#initial values
		vPow = 5.0 
		vDiv = 0.326
		vHigh = 0.0
		vLow = 0.0
		socHigh = 0.0
		socLow = 0.0
		readingValues = [(14.4, 1.0),
						(14.0, 0.99),
						(13.6, 0.99),
						(13.2, 0.31),
						(12.8, 0.08),
						(12.4, 0.04),
						(12.0, 0.02)
						]
		v = (read * vPow) / 1024.0 / vDiv + 0.23
		for i in readingValues:
			if v < i[0]:
				vHigh = i[0]
				socHigh = i[1]
			else:
				vLow = i[0]
				socLow = i[1]
			if vLow > 0:
				break
		chargeState = socLow + (v - vLow)*((socHigh - socLow)/(vHigh - vLow))
		return chargeState*100
            
        def destroy(self):
            self.data_log.close()
            

    def getPosition(self):
        #When triggered by interrupt:  Record Timestamp - Initial time in a list.
        writeNumber(0x69, 0x05)
        value = readNumber(0x69)
        print value
                

    def getTachos(channel, value):
        #Tacho I2C Port:  0x42
                        
        #When triggered by interrupt:  Record I2C bytes
        #I2C bytes:  4Bytes - timestamp.  2 Bytes - Velo   2 Bytes Position
        #Store I2C data into 3 variables to be used by state machine
        hyperbus.write_byte(0x42,0x05)
        val = hyperbus.read_i2c_block_data(0x42, 0x02, 10)
        #stuff = struct.unpack("<LHH",val)
        #stuff = self.bytesToNumber(self, val)
        #stuff = '{0:08x}{1:08x}'.format(val[0],val[1],val[2],val[3])
        print 'Tachos!:'
        print val
        return val

        
	def getData(self):
		sensor_data = [self.getAccl(), self.getGyro(), 
						self.getTemp(), self.getPres()]
		time = str(datetime.utcnow())
		self.writeDataFile(sensor_data, time)
		return sensor_data

	def tare(self, dofList, dofPos=None):
		if dofPos == None:
			dofPos = range(self.DOF)
		dofList = list(dofList)
		dofPos = list(dofList)
		while dofList and dofPos:
			self.zero[dofPos.pop(0)] += dofList.pop(0)

	def reset(self):
		for i in xrange(self.DOF): self.zero[i] = (0.0)

	def writeDataFile(self, sensor_data, time):
		self.data_log.write( "%s, " % time )
		for i in sensor_data:
			if hasattr(i, '__iter__'):
				for x in i:
					self.data_log.write("%.2f, " % x)
			elif (sensor_data[-1] == i):
				self.data_log.write("%.2f\n" % i)
			else:
				self.data_log.write("%.2f, " % i)

	
	
	
################################################################
################################################################

if __name__ == "__main__":
#	data = []
#	count = 0
#	term = 30
#	fstr = str(datetime.utcnow()) + " testdata.dat"
#	f = open(fstr,'w')
    tachos = testTacho()
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(21, GPIO.FALLING, callback=tachos.getTachos, bouncetime=5)
    GPIO.add_event_detect(20, GPIO.FALLING, callback=tachos.getTachos, bouncetime=15)
	sensors = PodSensors()
        print(sensors.getAccl())
#
#	while count < term*100:
#		print(sensors.getData())
#	count += 1
#	time.sleep(0.01)
