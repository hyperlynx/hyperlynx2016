int fDN = 5;
int fUP = 6;
int rDN = 9;
int rUP = 10;
int brk = 3;
int roll = 11;
int power = -250;
int brake = 250;
int count = 0;
int term = 30;

void setup() {
pinMode(fDN, OUTPUT);
pinMode(fUP, OUTPUT);
pinMode(rDN, OUTPUT);
pinMode(rUP, OUTPUT);
pinMode(brk, OUTPUT);
pinMode(roll, OUTPUT);
Serial.begin(9600);

}

void loop() {
  while (count < (term*100)){
    if (count % 300 == 0){
      if (brake > 0){
        analogWrite(roll, 0);
        analogWrite(brk, brake);
        brake = -brake;
      }
      if (brake < 0){
        analogWrite(brk, 0);
        analogWrite(roll, abs(brake));
        brake = -brake;
      }
    }
    if (count % 500 == 0){
      if (power > 0){
        analogWrite(fDN, 0);
        analogWrite(rDN, 0);
        analogWrite(fUP, power);
        analogWrite(rUP, power);
        power = -power;
      }
      if (power < 0){
        analogWrite(fUP, 0);
        analogWrite(rUP, 0);
        analogWrite(fDN, abs(power));
        analogWrite(rDN, abs(power));
        power = -power;
      }
    }
    Serial.println(count);
      count += 1;
      delay(10);
  }

}
