#include <Wire.h>

#define SLAVE_ADDRESS 0x09
int cmd = 0;
int dt = 6000;

int bFwd = 3;     // Brake forward pin
int bRev = 11;    // Brake reverse pin
int bPos = 1;     // Brake position pin
int fDfaFwd = 5;  // Front DDFA down pin
int fDfaRev = 6;  // Front DFA up pon
int fDfaPos = 3;  // Front DFA position pin
int rDfaFwd = 9;  // Rear DFA down pin
int rDfaRev = 10; // Rear DFA up pin
int rDfaPos = 4;  // Rear DFA position pin

boolean reset_arduino = false,brake_on = false , brake_off = false, send_brake = true;
boolean f_down = false, f_up = false, r_down = false, r_up = false;

unsigned short tstamp, diff;

void setup() {
//pinMode(13,OUTPUT);    //For testing LED
Serial.begin(115200);  //Start serial for output  

pinMode(bFwd, OUTPUT);
pinMode(bRev, OUTPUT);
pinMode(fDfaFwd, OUTPUT);
pinMode(fDfaRev, OUTPUT);
pinMode(rDfaFwd, OUTPUT);
pinMode(fDfaRev, OUTPUT);

Wire.begin(SLAVE_ADDRESS);  // initialize i2c as slave
Wire.onReceive(receiveData);
Wire.onRequest(sendData);

Serial.println("#2 Ready!");
}

void loop() {
  if (reset_arduino){
    reset_arduino = false;
    digitalWrite(10,HIGH); //Wired to reset pin
  }
  if (brake_on){
    delay(dt);
    brakeStop();
    brake_on = false;
  }
  if (brake_off){
    delay(dt);
    brakeStop();
    brake_off = false;
  }
  
  if (f_up){
    delay(dt);
    fStop();
    f_down = false;
  }
  if (f_down){
    delay(dt);
    fStop();
    f_down = false;
  }
  if (r_up){
    delay(dt);
    rStop();
    r_down = false;
  }
  if (r_down){
    delay(dt);
    rStop();
    r_down = false;
  }
  // put your main code here, to run repeatedly:
  // delay(1);
}

//callback for received data
void receiveData(int byteCount){

  while(Wire.available()){
    cmd = Wire.read();
    
    switch(cmd){
      case 0x00: // currently an empty command
        break; 
      case 0x01:    // Main brake on w/ stop
        brakeOn();
        break;
      case 0x02:    // Main brake off w/ stop
        brakeOff();
        break;
      case 0x03:    // Stop main brake
        brakeStop();
        break;
      case 0x04:    // Read main brake position
        cmd = readBrake();
        break;
      case 0x05:
        fDown();
        f_down = false;
        break;
      case 0x11:    // Front DFA down /w stop
        fDown();
        break;
      case 0x12:    // Front DFA up w/ stop
        fUp();
        break;
      case 0x13:    // Front DFA stop
        fStop();
        break;
      case 0x14:    // Read front DFA position
        readFront();
        break;
      case 0x15:    // Front DFA down no stop
        fDown();
        f_down = false;
        break;
      case 0x21:    // Rear DFA down w/ stop
        rDown();
        break;
      case 0x22:    // Rear DFA up w/ stop
        rUp();
        break;
      case 0x23:    // Rear DFA stop
        rStop();
        break;
      case 0x24:    // Read Rear DFA position
        readRear();
        break;
      case 0x25:    
        fDown();
        f_down = false;
        break;

    }
    
    Serial.print("data received #2: ");
    Serial.println(cmd);
  }
}


//callback for sending data
void sendData(){
 /* data[0] = tstamp >> 8;
  data[1] = tstamp & 0xFF;
  data[2] = diff >> 8;
  data[3] = diff & 0xFF;
  byte qty = 0x06;*/
  if (send_brake){
    cmd = bPos;
  }
  Serial.print(cmd);
  Wire.write(cmd);
}

void brakeOn()
{
  digitalWrite(bFwd, HIGH);
  digitalWrite(bRev, LOW);
  brake_on = true;
  //brakeStop();
}

void brakeOff()
{
  digitalWrite(bFwd, LOW);
  digitalWrite(bRev, HIGH);
  brake_off = true;
  //brakeStop();
}

void brakeStop()
{
  digitalWrite(bFwd, LOW);
  digitalWrite(bRev, LOW);
}

float readBrake()
{ // implement code for position into inches
  send_brake = true;
  return analogRead(bPos);
}

void fDown()
{
  digitalWrite(fDfaFwd, HIGH);
  digitalWrite(fDfaRev, LOW);
  //delay(dt);
  f_down = true;
  //fStop();
}

void fUp()
{
  digitalWrite(fDfaFwd, LOW);
  digitalWrite(fDfaRev, HIGH);
  //delay(dt);
  f_up = true;
  //fStop();
}

void fStop()
{
  digitalWrite(fDfaFwd, LOW);
  digitalWrite(fDfaRev, LOW);
}

float readFront()
{ // implement code for position into inches
  return analogRead(fDfaPos);
}

void rDown()
{
  digitalWrite(rDfaFwd, HIGH);
  digitalWrite(rDfaRev, LOW);
  r_down = true;
  //delay(4000);
  //fStop();
}

void rUp()
{
  digitalWrite(rDfaFwd, LOW);
  digitalWrite(rDfaRev, HIGH);
  r_up = true;
  //delay(4000);
  //fStop();
}

void rStop()
{
  digitalWrite(rDfaFwd, LOW);
  digitalWrite(rDfaRev, LOW);
}

float readRear()
{ // implement code for position into inches
  return analogRead(rDfaPos);
}

// void moveIt(int actuator, int direction)
// {
//   if (actuator == 1 && direction == 1)
//   {
//     digitalWrite(bFwd, HIGH);
//     digitalWrite(bRev, LOW);
//   }
//   else if (actuator == 1 && direction == 2)
//   {
//     digitalWrite(bFwd, LOW);
//     digitalWrite(bRev, HIGH);
//   }
//   else if (actuator == 1 && direction == 3)
//   {
//     digitalWrite(bFwd, LOW);
//     digitalWrite(bRev, LOW);
//   }
// }





// void move(int on, int off)
// {
//   digitalWrite(on, HIGH);
//   digitalWrite(off, LOW);
// }

// void stop(int one, int two)
// {
//   digitalWrite(one, LOW);
//   digitalWrite(two, LOW);
// }
