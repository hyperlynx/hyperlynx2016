/*
 * Authors: Nathan James and John Gorzynski
 * Date:    1/17/16
 * File:    IR_Tachometer
 * 
 * Tachometer code using an IR proximity detector
 * 
 * 
 * Notes: This has to start at low RPM to correctly set the delay range.
 *        Possibly the times between interrupts could be used if they
 *        less than 1 ms while on detection section (simpler solution)
 */


volatile byte REV;       //  VOLATILE DATA TYPE TO STORE REVOLUTIONS
volatile unsigned long lastISR = 0;

const int tdelay = 15;     // delay timing [ms]
const float wheelD = 1.9271;     // wheel diameter [ft]

unsigned long rpm;  //  DEFINE RPM AND MAXIMUM RPM

unsigned long spd;
unsigned long pos = 0;
unsigned long currtime;     // Define current time each loop
unsigned long t1me = 0;         //  DEFINE TIME TAKEN TO COVER ONE REVOLUTION

unsigned int line = 1;
 
//int ledPin = 13;           //   STATUS LED
 
//int led = 0;  //  INTEGERS TO STORE LED VALUE 
    
 void setup()
 {
     Serial.begin(115200);   // GET VALUES USING SERIAL MONITOR     
     attachInterrupt(digitalPinToInterrupt(2), RPMCount, LOW);     //  ADD A HIGH PRIORITY ACTION ( AN INTERRUPT)  WHEN THE SENSOR GOES FROM LOW TO HIGH
     
     REV = 0;      //  START ALL THE VARIABLES FROM 0     
     rpm = 0;     
     t1me = 0;
     
//     pinMode(ledPin, OUTPUT);     
     pinMode(3, OUTPUT);                
     pinMode(4, OUTPUT);     
     digitalWrite(3, HIGH);             //  VCC PIN FOR SENSOR     
     digitalWrite(4, LOW);              // GND PIN FOR SENSOR
     
 }
 
 void loop()
 {
  currtime = millis();                 // GET CURRENT TIME
     
    if(REV >= 1 )                      //  IT WILL UPDATE AFETR EVERY 5 READINGS
   {     
     rpm = 60000/(currtime - t1me)*REV;//  CALCULATE  RPM USING REVOLUTIONS AND ELAPSED TIME
     
    
     t1me = currtime;                            
     pos += 6.0214*REV;          //If this throws an error, include:  (unsigned int) 
     REV = 0;  

      //JOHN:   Add math for calculating velocity, print to Serial
//     spd = wheelD*PI*rpm/60;

     
     
     //JOHN:  Fix this!
     //Serial.printf("RPM: %s  Velo: %s  Pos:  %s \n", rpm, velocity, pos);
//     Serial.print("RPM: ");
     Serial.println(rpm);
//     Serial.print("Speed: ");
//     Serial.println(spd);

     // Testing Output
     //Serial.print(line++);
     //Serial.print(". ");
     //Serial.print(rpm);
     //Serial.print("   ");
     //Serial.print(pos);
     //Serial.println(" [ft]");


     
     
     //delay(500);
     }
   

//     Serial.print("Pin stat: ");                // check if sensor is detecting object
//     Serial.println(digitalRead(2));
 }
 
 void RPMCount()                                // EVERYTIME WHEN THE SENSOR GOES FROM LOW TO HIGH , THIS FUNCTION WILL BE INVOKED 
 {
  if((millis() - lastISR) > tdelay)
  {
   REV++;                                         // INCREASE REVOLUTIONS
//   Serial.println(REV);
//   Serial.print("   ");
//   Serial.println(lastISR);

//   if (led == LOW)
//   {
//     
//     led = HIGH;                                 //  TOGGLE STATUS LED
//   } 
//   
//   else
//   {
//     led = LOW;
//   }
//   digitalWrite(ledPin, led); 
  }

  lastISR = millis();
   
 }
//////////////////////////////////////////////////////////////  END OF THE PROGRAM  ///////////////////////////////////////////////////////////////////////
