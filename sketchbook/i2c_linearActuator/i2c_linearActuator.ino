#include <Wire.h>

#define SLAVE_ADDRESS 0x30
int number = 0;
int state = 0;

unsigned short tstamp, velo, pos;

void setup() {
//pinMode(13,OUTPUT);    //For testing LED
Serial.begin(115200);  //Start serial for output  
Wire.begin(SLAVE_ADDRESS);  // initialize i2c as slave

Wire.onReceive(receiveData);
Wire.onRequest(sendData);

Serial.println("#1 Ready!");
}

void loop() {
  delay(1);
}

//callback for received I2C data
void receiveData(int byteCount){

  while(Wire.available()){
    number = Wire.read();
    
    switch(number){
      case 0x00:
        //Initialize
        break; 
      case 0x01:
        //analogWrite
        break; 
      case 0x02:
        //digitalWrite
        break; 
      case 0x03:
        //analogRead
        break; 
       case 0x04:
        //digitalRead
        break; 
       case 0x05:
        //setPinMode
        break; 
       case 0x06:
        //returnString
        break; 
      case 0x07:
        //returnfloat
        break; 
        
    }
    Serial.print("data received: ");
    Serial.println(number);

  }
}

//callback for sending data
void sendData(){

byte data [6];
data[0] = tstamp >> 8;
data[1] = tstamp & 0xFF;
data[2] = velo >> 8;
data[3] = velo & 0xFF;
data[4] = pos >> 8;
data[5] = pos & 0xFF;
byte qty = 0x06;

  Wire.write(data,6);
}

