#include <DebugUtils.h>
#include <CommunicationUtils.h>
#include <FreeSixIMU.h>
#include <Wire.h>


float acc[3];
float mag;
float gyr[3];

FreeSixIMU imu = FreeSixIMU();


void setup() {
  Serial.begin(9600);
  Wire.begin();

  delay(5);
  imu.init();
  delay(5);

}

void loop() {
  imu.getValues(acc);
  delay(5);
  imu.getAngles(gyr);
  
  for(int i=0; i<3; i++)
  {
    //acc[i] = map(acc[i],232,286,3.5,4.3);
    acc[i] = acc[i]/256;
     
    Serial.print(acc[i]);
    Serial.print(" | ");
  }

  mag = sqrt(acc[0]*acc[0] + acc[1]*acc[1] + acc[2]*acc[2]);
  Serial.print(mag);
  Serial.print("  ||  ");
  
  for(int i=0; i<3; i++)
  {
    //gyr[i] = map(gyr[i],-232,286,-2,2);
    gyr[i] = gyr[i]/14.375;
    Serial.print(gyr[i]);
    Serial.print(" | ");
  }

  Serial.println();

  delay(100);
}
