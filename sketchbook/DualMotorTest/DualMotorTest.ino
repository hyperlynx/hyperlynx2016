int RPWM1 = 5;
int LPWM1 = 6;
int RPWM2 = 10;
int LPWM2 = 11;


void setup() {
  pinMode(RPWM1, OUTPUT);
  pinMode(LPWM1, OUTPUT);
  pinMode(RPWM2, OUTPUT);
  pinMode(LPWM2, OUTPUT);

}

void loop() {
  
  analogWrite(RPWM1, 255);
  analogWrite(LPWM1, 0);

  //delay(500);

  analogWrite(RPWM2, 255);
  analogWrite(LPWM2, 0);

  delay(700);

  analogWrite(RPWM1, 0);
  analogWrite(LPWM1, 0);

  //delay(500);

  analogWrite(RPWM2, 0);
  analogWrite(LPWM2, 0);

  delay(2000);

  analogWrite(RPWM1, 255);
  analogWrite(LPWM1, 0);

  analogWrite(RPWM2, 255);
  analogWrite(LPWM2, 0);

  delay(700);

  analogWrite(RPWM1, 0);
  analogWrite(LPWM1, 0);

  analogWrite(RPWM2, 0);
  analogWrite(LPWM2, 0);

  delay(2000);

  analogWrite(RPWM2, 0);
  analogWrite(LPWM2, 200);

  analogWrite(RPWM1, 0);
  analogWrite(LPWM1, 200);

  delay(3000);

}
