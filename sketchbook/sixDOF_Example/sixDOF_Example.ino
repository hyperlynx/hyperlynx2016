#include <FreeSixIMU.h>
#include <FIMU_ADXL345.h>
#include <FIMU_ITG3200.h>


#include <Wire.h>

float angles[3]; // yaw pitch roll
float accl[3];   // accelerometer

// Set the FreeSixIMU object
FreeSixIMU sixDOF = FreeSixIMU();

void setup() { 
  Serial.begin(9600);
  Wire.begin();
  
  delay(5);
  sixDOF.init(); //begin the IMU
  delay(5);
}

void loop() { 
  
  sixDOF.getAngles(angles);
  sixDOF.getQ(accl);

  Serial.print(accl[0]);
  Serial.print(" | ");  
  Serial.print(accl[1]);
  Serial.print(" | ");
  Serial.print(accl[2]);
  Serial.print(" |*| ");
  Serial.print(angles[0]);
  Serial.print(" | ");  
  Serial.print(angles[1]);
  Serial.print(" | ");
  Serial.println(angles[2]);

  //Serial.println();
   
  delay(100); 
}

