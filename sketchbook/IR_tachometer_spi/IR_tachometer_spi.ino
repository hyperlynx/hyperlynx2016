/*
 * Authors: Nathan James and John Gorzynski
 * Date:    1/17/16
 * File:    IR_Tachometer
 * 
 * Tachometer code using an IR proximity detector
 * 
 * 
 * Notes: This has to start at low RPM to correctly set the delay range.
 *        Possibly the times between interrupts could be used if they
 *        less than 1 ms while on detection section (simpler solution)
 */

// Original Example by Nick Gammon
// http://raspberrypi.stackexchange.com/questions/44277/spi-raspberry-pi-master-and-arduino-slave

#include <SPI.h>

char buf [100];
volatile byte loc;
volatile boolean send_data, start_tacho;

volatile unsigned short t, velo, pos;
float accel;

volatile byte REV;       //  VOLATILE DATA TYPE TO STORE REVOLUTIONS
volatile unsigned long lastISR = 0;

const int tdelay = 15;     // delay timing [ms]
const float wheelD = 1.9271;     // wheel diameter [ft]
volatile char *buffer;

unsigned long rpm;  //  DEFINE RPM AND MAXIMUM RPM

unsigned long spd;
unsigned long pos = 0;
unsigned long currtime;     // Define current time each loop
unsigned long t1me = 0;         //  DEFINE TIME TAKEN TO COVER ONE REVOLUTION

unsigned int line = 1;
 
//int ledPin = 13;           //   STATUS LED
 
//int led = 0;  //  INTEGERS TO STORE LED VALUE 
    
 void setup()
 {
    Serial.begin (250000);   // debugging
  
    // SPI: turn on SPI in slave mode
    SPCR |= _BV(SPE);
  
    // SPI: have to send on MISO - master in, slave out
    pinMode(MOSI, INPUT);
    pinMode(MISO, OUTPUT);
    pinMode(10, OUTPUT);  //The Cable Select or SS pin
    
    //SPI.begin();
    //SPI.setBitOrder(MSBFIRST);
    
    // SPI: get ready for an interrupt
    loc = 0;  // buffer empty
    send_data = false;
    start_tacho = false;
  
    // SPI: now turn on interrupts
    SPI.attachInterrupt();
     
     REV = 0;      //  START ALL THE VARIABLES FROM 0     
     rpm = 0;     
     t1me = 0;

    
 }

 // SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR;  // grab byte from SPI Data Register
  Serial.println("Something!");

  // add to buffer if theres room
  if (loc < sizeof buf)
  {
    if (c == 0xFF) {
      //Critical error - reboot arduino
    }
    if (c == 0xAA){  //Pi sends 0xAA first to start the tachometer
      Serial.println("Initialize Tachometer");
      start_tacho = true;
    }
    
    //buf [loc++] = c;
    //Serial.println(c);

    // ex: newline means time to process buffer
    if (c == '0x00')
      send_data = true;
  }
}  //end of interrupt routine SPI_STC_vect

 
 void loop()
 {
  currtime = millis();                 // GET CURRENT TIME
   
    if(REV >= 1 )                      //  IT WILL UPDATE AFETR EVERY 5 READINGS
   {     
     rpm = 60000/(currtime - t1me)*REV;//  CALCULATE  RPM USING REVOLUTIONS AND ELAPSED TIME
     
    
     t1me = currtime;                            
     pos += 6.0214*REV;          //If this throws an error, include:  (unsigned int) 
     REV = 0;  

      //JOHN:   Add math for calculating velocity, print to Serial
//     spd = wheelD*PI*rpm/60;

     
       sprintf(buffer, "RPM: %s  Velo: %s  Pos:  %s \n", rpm, spd, pos);
       for(int i=0; i<5; i++){
          Wire.write(*buffer);
       }
     
     
     //delay(500);
     }
   

//     Serial.print("Pin stat: ");                // check if sensor is detecting object
//     Serial.println(digitalRead(2));
 }
 
 void RPMCount()                                // EVERYTIME WHEN THE SENSOR GOES FROM LOW TO HIGH , THIS FUNCTION WILL BE INVOKED 
 {
  if((millis() - lastISR) > tdelay)
  {
   REV++;                                         // INCREASE REVOLUTIONS
  }

  lastISR = millis();
   
 }
 
//////////////////////////////////////////////////////////////  END OF THE PROGRAM  ///////////////////////////////////////////////////////////////////////
