// File:    SPI_Test
// Project: HyperLynx2016
// Author:  John Gorzynski
// Date:    1/21/2017

// Original Example by Nick Gammon
// http://raspberrypi.stackexchange.com/questions/44277/spi-raspberry-pi-master-and-arduino-slave

#include <SPI.h>

char buf [100];
volatile byte loc, command;
volatile boolean send_data, start_tacho;

volatile unsigned short t, velo, pos;
char temp[3];
float accel;
unsigned short velo_buff [10], velo_avg;
char *out;


void setup (void)
{
  Serial.begin (115200);   // debugging

  // SPI: turn on SPI in slave mode
  SPCR |= _BV(SPE);
  SPCR |= _BV(SPIE);

  // SPI: have to send on MISO - master in, slave out
  //pinMode(MOSI, INPUT);
  pinMode(MISO, OUTPUT);
  //pinMode(10, INPUT);  //The Cable Select or SS pin
  
  // SPI: get ready for an interrupt
  loc = 0;  // buffer empty
  send_data = false;
  start_tacho = false;

  // SPI: now turn on interrupts
  SPI.attachInterrupt();

  Serial.println("Arduino SPI Test is ready");

} //end of setup

// SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR;  // grab byte from SPI Data Register

  /*switch(command)
  {
    case 0xAA:
      command = c;
      SPDR = 0;
      start_tacho = true;
      Serial.print("Tacho Time!");
      break;

    case 0x05:
      SPDR = c + 100;
      send_data = true;
      Serial.print("Send the Tachos");
      break;

    case 0xFF:
      SPDR = 0;
      send_data = false;
      //t = 0;
      velo = 0;
      pos = 0;
  }*/

  // add to buffer if theres room
  if (loc < sizeof(buf) -1)
  {
    buf[loc++] = c;
    Serial.println(c);
  }
  send_data = true;
}  //end of interrupt routine SPI_STC_vect

// main loop - wait for flag set in interrupt routine
void loop (void)
{
  //RPi sends 0xAA to start tacho measurements
  //while(start_tacho){
    //Create test data based on time, velocity, and position

    
    //RPi sends 0x00 every 5 milliseconds to request current measurements
    if (send_data)
    {
      Serial.print("Something!");
      
      //digitalWrite(10,LOW);
      //SPI.transfer(0);
      temp[0] = 123;
      temp[1] = 55;
      temp[2] = 90;
      buf[loc] = 0;
      Serial.print("buf:");
      Serial.println(buf);
      Serial.print("pos:");
      Serial.println(temp);
              
      for (const char * p = "ABCD"; char w = *p; p++){
        SPI.transfer(w);
        delayMicroseconds(25);
      }
      //for(int i=0;i<sizeof(temp);i++){
      //  SPI.transfer(temp[i]);
      //}
      //digitalWrite(10,HIGH);
      buf[loc]=0;
      //Serial.println(0x10);
      loc = 0;
      send_data = false; 
    }

    //start_tacho = false;
    //}
  

}  //end of Main
