The select_echo_server.py and client.py programs work together.

Start the server program running first. python select_echo_server.py

Open another terminal tab and start the client program: python client.py

The client program sends a message to the server and the server echos it back.