# Sean Yu
# Downforce Actuator Pressure Sensor input test program
# Using GPIO

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN) # GPIO18 pin
#GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

while 1:
#    if GPIO.input(18):
    print(GPIO.input(18))
#    elif KeyboardInterrupt:
#        break

GPIO.cleanup()
