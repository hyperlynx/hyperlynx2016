#!/bash/python

import smbus
import time
# for RPI version 1, use 'bus = smbus.SMBus(0)'

bus = smbus.SMBus(1)

# This is the address set up on the Arduino
addr = 0x42

def write(cmd, adr = addr):
	bus.write_byte(adr,cmd)

def read(adr =addr):
	return bus.read_byte(adr)

def nRead(n, t=0.01, adr = addr):
	for i in range(n):
		print(read(adr))
		time.sleep(t)
