#Sean Yu
#CU Denver Hyperlynx Team
#LM35 Temperature Test Program

import RPi.GPIO as GPIO
import time
import datetime

import os
import glob
import subprocess

GPIO.setmode(GPIO.BCM)
GPIO.setup(2, GPIO.IN)

while True:
    print(GPIO.input(2))
