#!/bin/bash

# This will install python directories needed using pip

sudo apt-get update
sudo apt-get install build-essential python-pip python-dev python-smbus git tightvncserver

cp tightvncserver.service /etc/systemd/system/
sudo chown root:root /etc/systemd/system/tightvncserver.service
sudo chmod 755 /etc/systemd/system/tightvncserver.service
sudo systemctl enable tightvncserver.service


sudo python -m pip install PyMata


git clone https://github.com/adafruit/Adafruit_Python_GPIO.git
cd Adafruit_Python_GPIO
sudo python setup.py install

cd ../

. pathset.sh
# echo PYTHONPATH = $PYTHONPATH