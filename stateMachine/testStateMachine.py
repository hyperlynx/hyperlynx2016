''' Filename: testStateMachine.py

	Author: Diane Williams
	Date: 10/21/16

	Description:
		Creates StateMachine class object
		Creates a state for each state in state diagram
		Handles state transitions

		Tests each possible path through the state machine
		Displays the final state reached

	ToDo:
		add code for actions each state must perform/control

		add comments to code

'''

from stateMachine import StateMachine

# text strings to describe failure states
pod_failure_adjectives = [ "navigation", "power", "overheat", "electro_mechanical", "communication"]


# state transitions functions handle next state setting, depending on the txt string

def pod_standby_state_transitions(txt):
	splitted_txt = txt.split(None,1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")

	# only allowed transition is from standby to preflight
	if word == "preflight":
		newState = "pod_preflight_state"
	else:
		newState = "error_state"
	return (newState, txt)

def pod_preflight_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")

	# only allowed transition is from preflight to inflight_mode
	if word == "inflight":
		newState = "pod_inflight_state"
	else:
		newState = "error_state"
	return (newState, txt)

def pod_inflight_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "") 
	if word == "preliminary_braking":
		newState =  "pod_preliminary_braking_state"
	elif word in pod_failure_adjectives:
		newState = "pod_" + word + "_failure_state"
	else:
		newState = "error_state"
	return (newState, txt)

def pod_preliminary_braking_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")
	if word == "absolute_braking":
		newState = "pod_absolute_braking_state"
	else:
		newState = "error_state"
	return (newState, txt)

def pod_absolute_braking_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")
	if word == "postflight":
		newState = "pod_post_flight_state"
	else:
		newState = "error_state"
	return (newState, txt)

def pod_navigation_failure_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")
	if word == "absolute_braking":
		newState = "pod_absolute_braking_state"
	else:
		newState = "error_state"
	return (newState, txt)


def pod_power_failure_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")
	if word == "absolute_braking":
		newState = "pod_absolute_braking_state"
	else:
		newState = "error_state"
	return (newState, txt)

def pod_communication_failure_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")
	if word == "absolute_braking":
		newState = "pod_absolute_braking_state"
	else:
		newState = "error_state"
	return (newState, txt)

def pod_overheat_failure_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")

	if word == "absolute_braking":
		newState = "pod_absolute_braking_state"
	elif word in pod_failure_adjectives:
		newState = "pod_" + word + "_failure_state"
	else:
		newState = "error_state"
	return (newState, txt)

def pod_electro_mechanical_failure_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")
	if word == "absolute_braking":
		newState = "pod_absolute_braking_state"
	elif word == "complete":
		newState = "pod_complete_failure_state"
	else:
		newState = "error_state"
	return (newState, txt)

def post_flight_state(txt):
	return ("post_flight_state", "")


if __name__ == "__main__":

	#create StateMachine object
	m = StateMachine()

	# add all states
	m.add_state("pod_standby_state", pod_standby_state_transitions)
	m.add_state("pod_preflight_state", pod_preflight_state_transitions)
	m.add_state("pod_inflight_state", pod_inflight_state_transitions)
	m.add_state("pod_preliminary_braking_state", pod_preliminary_braking_state_transitions)
	m.add_state("pod_absolute_braking_state", pod_absolute_braking_state_transitions)
	m.add_state("pod_navigation_failure_state", pod_navigation_failure_state_transitions)
	m.add_state("pod_power_failure_state", pod_power_failure_state_transitions)
	m.add_state("pod_communication_failure_state", pod_communication_failure_state_transitions)
	m.add_state("pod_overheat_failure_state", pod_overheat_failure_state_transitions)
	m.add_state("pod_electro_mechanical_failure_state", pod_electro_mechanical_failure_state_transitions)
	m.add_state("pod_complete_failure_state", None, end_state=1)
	m.add_state("pod_post_flight_state", None, end_state=1)
	m.add_state("error_state", None, end_state=1)

	# must set the start state
	m.set_start("pod_standby_state")

	# test all paths through state machine
	m.run("preflight inflight preliminary_braking absolute_braking postflight")
	m.run("preflight inflight navigation absolute_braking postflight")
	m.run("preflight inflight power absolute_braking postflight")
	m.run("preflight inflight communication absolute_braking postflight")
	m.run("preflight inflight overheat absolute_braking postflight")
	m.run("preflight inflight overheat electro_mechanical complete")
	m.run("preflight inflight electro_mechanical absolute_braking postflight")
	m.run("preflight inflight electro_mechanical complete")
	
